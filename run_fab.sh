#!/usr/bin/bash

fab host_info
fab -P update_apt
fab -P install_git
fab -P install_java
fab -P setup_ipmi
fab -P assign_ip
fab -P ofed_download
fab -P install_ofed
fab -P restart

sleep 180

fab -P check_ofed
fab -P configure_hugepages
fab -P init_shm
fab -P load_modules
fab -P copy_file
fab -P load_modules
fab -P check_modules
fab check_ibvdevices
fab -P copy_project
fab -P copy_server_file
fab -P install_project
fab -P configure_zipf
